// Load the AWS SDK for Node.js
const AWS = require('aws-sdk');
// Set region
AWS.config.update({ region: 'ap-southeast-2' });

exports.handler = async function (event, context, callback) {

    const sns = new AWS.SNS({ apiVersion: '2010-03-31' });

    sns.setSMSAttributes(
        {
            attributes: {
                DefaultSMSType: "Transactional"
            }
        },
        function (error) {
            if (error) {
                console.log(error);
            }
        }
    );

    await Promise.all([sns.publish(createParam('+61413585858')).promise(), sns.publish(createParam('+61488776906')).promise()])

    callback(null, { "message": "Successfully executed" });
}

const createParam = (phoneNumber) => {
    return {
        Message: 'Motion detected at home! 警報器響啦!',
        PhoneNumber: phoneNumber
    };
}